angular.module('aplicacao').service('MessageService', function(){
    
    /**
      * Show result message to user.
      * @param {*} message 
      */
    this.addMessage = function (message) {
        Materialize.toast(message, 2000);
    };
});