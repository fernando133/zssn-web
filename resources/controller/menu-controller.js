angular.module('aplicacao').controller('MenuController', function($scope, $state){
    /**
     * Navigate to the new survivor page.
     */
    $scope.goStart = function() {
        $state.go('aplication_start');
    }

    /**
     * Navigate to trade functions page.
     */
    $scope.goTrade = function() {
        $state.go('aplication_trade');
    }
});
