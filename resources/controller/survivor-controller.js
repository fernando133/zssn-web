angular.module('aplicacao').controller('SurvivorController', function($scope, $resource, $http, MessageService, $q){
    
     $scope.water = {'id':1, 'name': 'water', 'points':4};
     $scope.food = {'id':2, 'name':'food', 'points':3};
     $scope.medication ={"id":3, 'name':'medication', 'points':2};
     $scope.ammunition ={"id":4, 'name':'ammunition', 'points':1};

     /**
      * Start the use case. 
      */
     var init = function() {
        $scope.register = true;
        $scope.survivor={};
        $scope.listSurvivors = [];
        $scope.inventory={};
        $scope.survivor.inventoryList = [];
        getAllSurvivors();
        $scope.survivorA = {};
        $scope.survivorB = {};
        $scope.isSurvivorA = false;
        $scope.isSurvivorB = false;
        $scope.totalPointsTradeA = 0;
        $scope.totalPointsTradeB = 0;
    }

    /**
     * Update a new location for survivor A.
     */
    $scope.updateLocation = function(survivor, type) {
        var $setLocation = $q.defer();
        setUpLocation(survivor, $setLocation);
        
        $setLocation.promise.then(function() {
            if(type == 'A') {
                getSurvivorAbyId($scope.survivorA.id);
            }else if(type == 'B') {
                getSurvivorBbyId($scope.survivorB.id);
            }
        });
    };
    

    /**
     * Calcule a new location for given survivor.
     */
    var setUpLocation = function (survivor, $setLocation) {
        var lat = Math.floor(Math.random()*199) - 99;
        var lon = Math.floor(Math.random()*199) - 99;

        var locationTO = {
            "survivor" : survivor,
            "lat": lat,
            "lon": lon
        }
        $http.post("http://localhost:8080/zssn/survivor/updateLocation", locationTO).then(function(response) {
            $scope.message=response.data.message;
            MessageService.addMessage($scope.message);
        });
         $setLocation.resolve();
    }

    /**
     * Mark a survivor as infected
     */
    $scope.markAsInfected = function (survivor, type) {
        var $infected = $q.defer();
        setInfected(survivor, $infected);
        
        $infected.promise.then(function() {
            if(type == 'A') {
                getSurvivorAbyId($scope.survivorA.id);
            }else if(type == 'B') {
                getSurvivorBbyId($scope.survivorB.id);
            }
        });

    };

    /**
     * Mark a given survivor as infected.
     */
    var setInfected = function (survivor, $infected) {
        $http.post("http://localhost:8080/zssn/survivor/markAsInfected", survivor).then(function(response) {
            $scope.message=response.data.message;
            MessageService.addMessage($scope.message);
        });
        $infected.resolve();
    }

    /**
     * Get survivor a by an specific informed id.
     */
    var getSurvivorAbyId = function(survivorId) {
        $http.get("http://localhost:8080/zssn/survivor/" + survivorId).then(function(response) {
            $scope.survivorA = angular.copy(response.data);
        });
        
    };

    /**
     * Get survivor b by an specific informed id.
     */
    var getSurvivorBbyId = function(survivorId) {
        $http.get("http://localhost:8080/zssn/survivor/" + survivorId).then(function(response) {
            $scope.survivorB = angular.copy(response.data);
        });
        
    };

     /**
      * 
      */
     $scope.setSurvivorA = function(survivor) {
        $scope.survivorA = angular.copy(survivor);
        $scope.isSurvivorA = true;
     };

     /**
      * 
      */
     $scope.setSurvivorB = function(survivor) {
        $scope.survivorB = angular.copy(survivor);
        $scope.isSurvivorB = true;
     }
    
     /**
      * Return a list of all survivors.
      */
     var getAllSurvivors = function() {
          $http.get("http://localhost:8080/zssn/survivor/list").then(function(response) {
            $scope.listSurvivors = response.data;
          });
     };

     /**
      * Get location for survivor in the moment of registration.
      */
     $scope.getLocation = function () {
        $scope.survivor.lat = Math.floor(Math.random()*199) - 99;
        $scope.survivor.lon = Math.floor(Math.random()*199) - 99;
     };

     /**
      * Submit the survivor informartion.
      */
     $scope.submitSurvivorInformation = function() {
        var $buildInventory = $q.defer();
        $scope.buildInventoryForSurvivor($buildInventory);
        $buildInventory.promise.then(function() {
            $http.post("http://localhost:8080/zssn/survivor/create", $scope.survivor).then(function(response) {
                $scope.message=response.data.message;
                init();
                MessageService.addMessage($scope.message);
            });
        });
     };

     /**
      * Make trade of itens between two survivors.
      */
     $scope.makeTradeOfItens = function() {
         console.log("make trade");
        var $buildTrade = $q.defer();
        buildTradeTO($buildTrade);
        $buildTrade.promise.then(function() {
            $http.post("http://localhost:8080/zssn/inventory/makeTrade", $scope.tradeTO).then(function(response) {
                $scope.message=response.data.message;
                MessageService.addMessage($scope.message);
            });
        });
     }

     /**
      * Build the trade transport object to send.
      */
     var buildTradeTO = function($buildTrade) {

        console.log("build trade");
        var survivorAitens = [];
        var survivorBitens = [];

        var waterInventoryA = {"item":$scope.water, "amount":$scope.tradeWaterA};
        var foodInventoryA = {"item":$scope.food, "amount":$scope.tradeFoodA};
        var medicationInventoryA = {"item":$scope.medication, "amount":$scope.tradeMedicationA};
        var ammunitionInventoryA = {"item":$scope.ammunition, "amount": $scope.tradeAmmunitionA};

        survivorAitens.push(waterInventoryA);
        survivorAitens.push(foodInventoryA);
        survivorAitens.push(medicationInventoryA);
        survivorAitens.push(ammunitionInventoryA);

        var waterInventoryB = {"item":$scope.water, "amount":$scope.tradeWaterB};
        var foodInventoryB = {"item":$scope.food, "amount":$scope.tradeFoodB};
        var medicationInventoryB = {"item":$scope.medication, "amount":$scope.tradeMedicationB};
        var ammunitionInventoryB = {"item":$scope.ammunition, "amount": $scope.tradeAmmunitionB};

        survivorBitens.push(waterInventoryB);
        survivorBitens.push(foodInventoryB);
        survivorBitens.push(medicationInventoryB);
        survivorBitens.push(ammunitionInventoryB);

         $scope.tradeTO = {
             firstSurvivor : $scope.survivorA,
             secondSurvivor : $scope.survivorB,
             firstSurvivorItens : survivorAitens,
             secondSurvivorItens : survivorBitens
         }
          $buildTrade.resolve();
     }

     /**
      * Build the start list of iventory for the survivor registration.
      */
     $scope.buildInventoryForSurvivor = function($buildInventory) {
        
        var waterInventory = {"item":$scope.water, "amount":$scope.inventory.water};
        var foodInventory = {"item":$scope.food, "amount":$scope.inventory.food};
        var medicationInventory = {"item":$scope.medication, "amount":$scope.inventory.medication};
        var ammunitionInventory = {"item":$scope.ammunition, "amount": $scope.inventory.ammunition};

        var listInventory = [];

        listInventory.push(waterInventory);
        listInventory.push(foodInventory);
        listInventory.push(medicationInventory);
        listInventory.push(ammunitionInventory);

        $scope.survivor.inventoryList = listInventory;

        $buildInventory.resolve();
     };

     /**
      * 
      */
     $scope.calculatePointsWaterA = function(tradeWaterA) {
        $scope.subTotalPointsWaterA = $scope.water.points * tradeWaterA;
     };

     /**
      * 
      */
     $scope.calculatePointsFoodA = function(tradeFoodA) {
        $scope.subTotalPointsFoodA = $scope.food.points * tradeFoodA;
     }

     /**
      * 
      */
     $scope.calculatePointsMedicationA = function(tradeMedicationA) {
         $scope.subTotalPointsMedicationA = $scope.medication.points * tradeMedicationA;
     }

     /**
      * 
      */
     $scope.calculatePointsAmmunitionA = function (tradeAmmunitionA) {
         $scope.subTotalPointsAmmunitionA = $scope.ammunition.points * tradeAmmunitionA;
     }

      /**
      * 
      */
     $scope.calculatePointsWaterB = function(tradeWaterB) {
        $scope.subTotalPointsWaterB = $scope.water.points * tradeWaterB;
     };

     /**
      * 
      */
     $scope.calculatePointsFoodB = function(tradeFoodB) {
        $scope.subTotalPointsFoodB = $scope.food.points * tradeFoodB;
     }

     /**
      * 
      */
     $scope.calculatePointsMedicationB = function(tradeMedicationB) {
         $scope.subTotalPointsMedicationB = $scope.medication.points * tradeMedicationB;
     }

     /**
      * 
      */
     $scope.calculatePointsAmmunitionB = function (tradeAmmunitionB) {
         $scope.subTotalPointsAmmunitionB = $scope.ammunition.points * tradeAmmunitionB;
     }

    init();
});