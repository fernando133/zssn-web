var app = angular.module('aplicacao', ['ngResource', 'ui.router'])
    .config(function($stateProvider){
        $stateProvider
            .state('aplication_start',{
                name: 'aplication_start',
                url: 'home',
                templateUrl: 'resources/pages/survivor/form-survivor.html'
            }).state('aplication_trade',{
                name: 'aplication_trade',
                url: 'trade-itens',
                templateUrl: 'resources/pages/survivor/trade-itens.html'
            });
    });

    app.run(function($rootScope, $state){
        $state.go('aplication_start');
    });